from typing import Any
from matplotlib import ticker
import matplotlib.pyplot as plt
from datetime import datetime
from re import sub
import os

# Source for better log-scale axis: 
# https://stackoverflow.com/questions/61351547/how-to-remove-scientific-notation-from-a-log-log-plot

SAVE = True # Set to true to save plots to file

plt.style.use('seaborn-bright')
plt.rcParams["font.family"] = "serif"
plt.rcParams["font.serif"] = ["Times New Roman"]

# The following lines create solid, dashed, and dotted line styles
# using the following format: (offset, (dash size, space size))
line_styles = [
    (0,   ()),           # solid
    (0.5, (5, 1)),       # dashed
    (0,   (1.2, 0.6))    # dotted
]

line_colors = [
    #"b", "g", "r", "m", "#ff7f0e", "#17becf"
    "#E69F00", "#CC5997", "#009E73", "#0072B2", "#56B4E9", "#D55E00"
]

folder_name = "plots_{:%Y%m%d_%H%M%S}".format(datetime.now())
if SAVE: os.mkdir(folder_name)

plot_count = 0

def snake_case(s):
    """
    Modified from https://www.w3resource.com/python-exercises/string/python-data-type-string-exercise-97.php
    """
    return sub("[^A-Za-z0-9_]", "", '_'.join(
        sub('([A-Z][a-z]+)', r' \1',
        sub('([A-Z]+)', r' \1',
        s.replace('-', ' '))).split()
    ).lower())

class TrackedVariables():
    """
    Provides a simple way of tracking the value of a variable as it changes.

    Example:

    ```
    tv = TrackedVariables()
    tv.x = 5
    tv.x += 10
    tv.x *= 2
    ```

    Now, the output of `tv.log.x` should be `[5, 15, 30]`.
    """

    class DummyLogClass:
        """
        Needed to implement the `tv.log.x` behavior.
        """
        def __init__(self, _parent: 'TrackedVariables') -> None:
            self._parent = _parent
        def __getattr__(self, __name: str) -> Any:
            if __name != "_parent":
                return self._parent._history[__name]
            return self.__dict__["_parent"]

    def __init__(self):
        self.__dict__["_history"] = {}
        self.__dict__["log"] = self.DummyLogClass(self)

    def prohibit_name(self, name):
        raise KeyError(f"`{name}` is a reserved name for the "
        + "TrackedVariables class and cannot be used")

    def clear(self, name: str) -> None:
        self._history[name] = []
        self.__dict__[name] = None

    def clear_all(self):
        for name in self.__dict__:
            self.__dict__[name] = None
        self.__dict__["_history"] = {}

    def __setattr__(self, __name: str, __value: Any) -> None:
        if __name == "_history" or __name == "_parent" or __name =="log":
            self.prohibit_name(__name)
        else:
            if __name not in self._history.keys():
                self._history[__name] = []
            self._history[__name].append(__value)
            self.__dict__[__name] = __value

class PlotObject:
    def __init__(self, plt: plt, ax1, ax2, fig, title) -> None:
        self.plt = plt
        self.ax1 = ax1
        self.ax2 = ax2
        self.fig = fig
        self.title = title
    def done(self):
        """
        Saves or shows the plot.
        """
        global plot_count
        if SAVE: 
            plt.savefig(os.path.join(folder_name, 
                f"{plot_count + 1:03d}{self.title}"), dpi=300)
        else:
            plt.show()
        plt.close()
        plot_count += 1   

def plot(datasets, title, axis_labels = ["", "", ""], is_log: bool = False,
        split_at = 0, legend_pos = ["lower left", "lower right"],
        subplots = True, line_colors = line_colors):
    """
    Note: `datasets` expects a series like [(X1, Y1, label?), (X2, Y2, label?)] 
    where each of X and Y is a list.

    The `secondary_scale` parameter allows the last X datasets to be plotted
    on an alternative y-axis.
    """
    use_legend = False

    if subplots and split_at > 0: 
        fig, (ax1, ax2) = plt.subplots(2, 1)
        fig.set_size_inches((6.4, 6.4))
    else:
        fig, ax1 = plt.subplots()
        ax2 = None

        if split_at > 0:
            ax2 = ax1.twinx()

    for index, data in enumerate(datasets):
        if len(data) == 3:
            X, Y, legend_label = data
            use_legend = True # Only create a legend if there is at least
                              # one manually labeled entry in the data
        else:
            X, Y = data
            legend_label = ""

        if len(datasets) - index <= split_at:
            plot_target = ax2
        else:
            plot_target = ax1

        plot_target.plot(X, Y, line_colors[index % len(line_colors)], lw=2,
            label=legend_label, linestyle=line_styles[index % len(line_styles)])

    ax1.set_title(title)

    if title != "": title = "_" + snake_case(title)

    if use_legend: 
        ax1.legend(loc=legend_pos[0])
        if split_at > 0: 
            ax2.legend(loc=legend_pos[1])

    ax1.minorticks_on()
    if not subplots or split_at <= 0: ax1.set_xlabel(axis_labels[0])
    ax1.set_ylabel(axis_labels[1])

    if split_at > 0:
        ax2.set_ylabel(axis_labels[2])

    ax1.grid(which="minor", lw=0.2)
    ax1.grid(which="major", color="k", lw=0.5)

    if subplots and split_at > 0:
        ax2.minorticks_on()
        ax2.set_xlabel(axis_labels[0])
        ax2.set_ylabel(axis_labels[2])
        ax2.grid(which="minor", lw=0.2)
        ax2.grid(which="major", color="k", lw=0.5)
        

    if is_log:
        ax1.set_yscale('log')
        if split_at > 0:
            ax2.set_yscale('log')
        # ax.yaxis.set_major_formatter(ticker.ScalarFormatter())
        # ax.ticklabel_format(style='sci', scilimits=(-6, 9)) 
    else:
        # Start the X and Y axes at 0, if the minimum of all datasets is nonnegative
        if split_at > 0: axes = [ax1, ax2]
        else: axes = [ax1]
        for ax in axes:
            prev_xlim = ax.get_xlim()
            prev_ylim = ax.get_ylim()
            if min([min(d[0]) for d in datasets]) >= 0: ax.set_xlim(0, prev_xlim[1])
            if min([min(d[1]) for d in datasets]) >= 0: ax.set_ylim(0, prev_ylim[1])

    return PlotObject(plt, ax1, ax2, fig, title)
