import matplotlib.pyplot as plt

from math import log
ln2 = log(2)
t_half = 1 # hour
dt = 0.001 # hour
t_end = 24 # hour

p = 1
t = 0

T = []
P = [] # mg

# dP_dt = lambda P: 1.98 * P * (1- (P / 1e14))
dP_dt = lambda P: 1.98 * P * (1- (P / 1e14)) - (0.385 * P)

while t < t_end:

    dP = dt * dP_dt(p)  
    
    T.append(t)
    P.append(p)
    
    p += dP
    t += dt

plt.plot(T, P)
plt.show()
