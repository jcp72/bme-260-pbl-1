import matplotlib.pyplot as plt

from math import log

t_half = 1 # hour
dt = 0.001 # hour
t_end = 25 # hour

dosage_interval = 5 # hours, (administer the drug every `dosage_interval` hours)
dosage_time = 0.25  # hours (administer the drug over a `dosage_time` period)

a = 0
t = 0

### END MODIFIABLE PARAMETERS ###

num_dosage_steps = (dosage_time / dt)

T = []
A = [] # mg

ln2 = log(2)
dA_dt = lambda A: - ln2 / t_half * A

while t < t_end:

    dA = dt * dA_dt(a)  
    
    T.append(t)
    A.append(a)

    if (t % dosage_interval) < dosage_time:
        a += 250 / num_dosage_steps # mg
    
    a += dA
    t += dt

plt.plot(T, A)
plt.show()
