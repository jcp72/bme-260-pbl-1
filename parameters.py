dt = 0.001  # hour
t_end = 120 # hour

incubation_period = 20 # hour

dosage_amount = 75  # mg
dosage_interval = 6 # hours, (administer the drug every `dosage_interval` hours)
dosage_time = 0.25  # hours (administer the drug over a `dosage_time` period)

# Rate contants (source: Lewis)
# k_12 = 0.401 # 1/hr
# k_21 = 0.730 # 1/hr
k_el = 1.71  # 1/hr
k_e  = 1.57  # 1/hr

V_C = 10 # L (based on 70kg Vc = 20 L, need to look this up)

# Cc = drug concentration in central compartment 
# Cp = drug concentration in peripheral compartment

# dCc_dt = lambda tv: - (k_12 + k_el) * tv.Cc + k_21 * tv.Cp 
# dCp_dt = lambda tv: k_12 * tv.Cc - k_21 * tv.Cp 

P_0 = 1e3 # CFU, arbitrary choice of number
L = 1e14  # bacteria... need to double-check this figure

# Cl = concentration of drug in liver
# Cc = concentration of drug in colon
# Ck = concentration of drug in kidneys

# Arbitrary rate constants (units: 1/hr) VERY VERY ARBITRARY
k_2 = 0.9
k_3 = 1.1
k_4 = 1.3

# Bacterial growth constants
k_0 = 1.7
C_0 = 1.30

# k_0 - k_a max, chosen arbitrarily based on ampicillin killing paper
k_diff_max = 6

antibiotic_resistant_fraction = 0.01
C_0_r = 2.2

protein_binding = 0.1
