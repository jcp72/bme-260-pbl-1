from util import *
from parameters import *
from statistics import mean
import util

def run_sim(
        dt, t_end, incubation_period, dosage_amount, dosage_interval, 
        dosage_time, k_el, k_e, V_C, P_0, L, k_2, k_3, k_4, k_0, C_0, 
        k_diff_max, antibiotic_resistant_fraction, C_0_r, protein_binding
    ):
    
    tv = TrackedVariables()

    tv.t  = 0
    tv.Cl = 0
    tv.Cc = 0
    tv.Ck = 0
    tv.P  = P_0 * (1 - antibiotic_resistant_fraction)
    tv.Pr = P_0 * antibiotic_resistant_fraction

    unbound_fraction = 1 - protein_binding

    k_m = k_el - k_e
    num_dosage_steps = (dosage_time / dt)

    while tv.t < t_end:

        dCl = (k_4 * tv.Ck - (k_2 + k_m) * tv.Cl) * dt
        dCc = (k_2 * tv.Cl -  k_3        * tv.Cc) * dt
        dCk = (k_3 * tv.Cc - (k_4 + k_e) * tv.Ck) * dt

        if tv.t > incubation_period:

            # antibiotic-hindered bacterial growth
            k_eff = k_0 - max(0,                # don't grow faster than k_0 ...
                k_diff_max * (unbound_fraction * tv.Cc - C_0)) 

            # antibiotic-hindered bacterial growth
            k_eff_r = k_0 - max(0,                  # don't grow faster than k_0 ...
                k_diff_max * (unbound_fraction * tv.Cc - C_0_r)) 

            if (tv.t % dosage_interval) < dosage_time:
                dCl += (dosage_amount / num_dosage_steps) / V_C # mcg / mL
        else:

            k_eff   = k_0 # logistic baterial growth
            k_eff_r = k_0 # logistic growth for antibiotic resistant bacteria

        dP  = k_eff   * tv.P  * (1 - tv.P  / L) * dt
        # dPr = k_0     * tv.Pr * (1 - tv.Pr / L) * dt
        dPr = k_eff_r * tv.Pr * (1 - tv.Pr / L) * dt
        
        tv.Cl += dCl
        tv.Cc += dCc
        tv.Ck += dCk

        if tv.P + dP < 1: tv.P = 0
        else: tv.P  += dP

        if tv.Pr + dPr < 1: tv.Pr = 0
        else: tv.Pr  += dPr

        tv.t += dt

    return tv

def update_defaults():
    run_sim.__defaults__ = (dt, t_end, incubation_period, dosage_amount, 
        dosage_interval, dosage_time, k_el, k_e, V_C, P_0, L, k_2, k_3, k_4, 
        k_0, C_0, k_diff_max, antibiotic_resistant_fraction, C_0_r, 
        protein_binding)

def demo_01():

    tv = run_sim()

    p = plot([
            (tv.log.t, tv.log.Cl, "liver"), 
            (tv.log.t, tv.log.Cc, "colon"), 
            (tv.log.t, tv.log.Ck, "kidneys"),
            (tv.log.t, [a + b for (a, b) in zip(tv.log.P, tv.log.Pr)], "total bacteria"),
            (tv.log.t, tv.log.P, "susceptible bacteria"),
            (tv.log.t, tv.log.Pr, "resistant bacteria")
        ], "Drug in liver, colon, and kidneys affects bacterial population",
        ["time (hours)", "Ampicillin concentration (\u00b5g/mL)", 
            "Bacterial population (CFU)"], 
        is_log=True,
        split_at=3,
        legend_pos=["lower left", "lower left"]
    )

    p.ax1.set_ylim((1e-1, 5.5e1))
    p.done()

def analyze_relative_rates():

    for r in range(10, 100, 10):
        k_2 = 0.9 * r / 10
        k_3 = 1.1 * r / 10
        k_4 = 1.3 * r / 10

        tv = run_sim(
            incubation_period=0,
            t_end = 24,
            k_2 = k_2,
            k_3 = k_3,
            k_4 = k_4
        )

        p = plot([
                (tv.log.t, tv.log.Cl, "liver"), 
                (tv.log.t, tv.log.Cc, "colon"), 
                (tv.log.t, tv.log.Ck, "kidneys")
            ], f"Drug in compartments for r = {r/10:2.1f}",
            ["time (hours)", "Ampicillin concentration (\u00b5g/mL)"], 
            is_log=True,
            legend_pos=["lower left"]
        )

        p.ax1.set_ylim((1e-1, 1e1))
        p.done()

def analyze_relative_rates_2():

    averages = {
        "liver": [],
        "colon": [],
        "kidneys": []
    }

    r_values = []

    for r in range(10, 100, 10):
        k_2 = 0.9 * r / 10
        k_3 = 1.1 * r / 10
        k_4 = 1.3 * r / 10

        tv = run_sim(
            incubation_period=0,
            t_end = 24,
            k_2 = k_2,
            k_3 = k_3,
            k_4 = k_4
        )

        r_values.append(r/10)
        averages["liver"].append(mean(tv.log.Cl))
        averages["colon"].append(mean(tv.log.Cc))
        averages["kidneys"].append(mean(tv.log.Ck))

    p = plot([
            (r_values, averages["liver"], "liver"), 
            (r_values, averages["colon"], "colon"), 
            (r_values, averages["kidneys"], "kidneys")
        ], f"Average concentration of drug versus scaled \"r\"",
        ["r-value (unitless)", "Ampicillin concentration (\u00b5g/mL)"], 
        is_log=False,
        legend_pos=["lower left"]
    )

    p.done()

def analyze_relative_rates_3():

    for r in range(10, 100, 10):
        k_2 = 0.9 / (r / 10)
        k_3 = 1.1 / (r / 10)
        k_4 = 1.3 / (r / 10)

        tv = run_sim(
            incubation_period=0,
            t_end = 24,
            k_2 = k_2,
            k_3 = k_3,
            k_4 = k_4
        )

        p = plot([
                (tv.log.t, tv.log.Cl, "liver"), 
                (tv.log.t, tv.log.Cc, "colon"), 
                (tv.log.t, tv.log.Ck, "kidneys")
            ], f"Drug in compartments for r = {r/10:2.1f}",
            ["time (hours)", "Ampicillin concentration (\u00b5g/mL)"], 
            is_log=False,
            legend_pos=["lower left"]
        )

        p.ax1.set_ylim((1e-1, 1e1))
        p.done()

def analyze_relative_rates_4():

    averages = {
        "liver": [],
        "colon": [],
        "kidneys": []
    }

    r_values = []

    for r in range(10, 100, 10):
        k_2 = 0.9 / (r / 10)
        k_3 = 1.1 / (r / 10)
        k_4 = 1.3 / (r / 10)

        tv = run_sim(
            incubation_period=0,
            t_end = 24,
            k_2 = k_2,
            k_3 = k_3,
            k_4 = k_4
        )

        r_values.append(r/10)
        averages["liver"].append(mean(tv.log.Cl))
        averages["colon"].append(mean(tv.log.Cc))
        averages["kidneys"].append(mean(tv.log.Ck))

    p = plot([
            (r_values, averages["liver"], "liver"), 
            (r_values, averages["colon"], "colon"), 
            (r_values, averages["kidneys"], "kidneys")
        ], f"Average concentration of drug versus scaled \"1/r\"",
        ["r-value (unitless)", "Ampicillin concentration (\u00b5g/mL)"], 
        is_log=False,
        legend_pos=["lower left"]
    )

    p.done()

def test_drug():

    tv = run_sim(
        incubation_period=0,
        t_end=24,
    )

    p = plot([
            (tv.log.t, tv.log.Cl, "liver"), 
            (tv.log.t, tv.log.Cc, "colon"), 
            (tv.log.t, tv.log.Ck, "kidneys")
        ], "Concentration of drug oscillates above baseline value",
        ["time (hours)", "Ampicillin concentration (\u00b5g/mL)"], 
        is_log=False,
        legend_pos=["lower left"]
    )

    p.fig.subplots_adjust(bottom=0.2) # or whatever

    p.plt.figtext(0.15, 0.03, f"Average concentrations: " + 
        f"liver {mean(tv.log.Cl):.2f} \u00b5g/mL, " + 
        f"colon {mean(tv.log.Cc):.2f} \u00b5g/mL, " +
        f"kidneys {mean(tv.log.Ck):.2f} \u00b5g/mL")
    
    p.done()

def use_updated_params():
    global k_2, k_3, k_4, dosage_amount, dosage_interval, V_C, C_0_r

    k_2 = 0.58
    k_3 = 5.8
    k_4 = 0.5

    dosage_amount = 250
    dosage_interval = 6
    V_C = 5
    C_0_r = 1.65

    update_defaults()

def main(): 

    update_defaults()

    # NOTE: only manually set plot_count if you are running a subset of 
    # the below functions

    analyze_relative_rates()   # 9 plots
    analyze_relative_rates_2() # 1 plot
    analyze_relative_rates_3() # 9 plots
    analyze_relative_rates_4() # 1 plot

    # Changes to the parameters were agreed upon after analyzing the first
    # 20 graphs plotted.
    use_updated_params()
    
    test_drug() # 1 plot

    util.plot_count = 21
    demo_01()

if __name__ == "__main__":
    main()
